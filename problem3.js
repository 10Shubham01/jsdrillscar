const sortedCarModel = (inventory) => {
  if (inventory) {
    let carModel = inventory.sort((a, b) =>
      a.car_model > b.car_model ? 1 : -1
    );
    for (let index = 0; index < carModel.length; index++) {
      console.log(carModel[index].car_model);
    }
  }
};
module.exports = sortedCarModel;
