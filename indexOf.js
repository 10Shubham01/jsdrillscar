const inv = require("./inventory");
let car = {
  id: 4,
  car_make: "Honda",
  car_model: "Accord",
  car_year: 1983,
};

function indexOf(inventory, car) {
  for (let index = 0; index < inventory.length; index++) {
    if (inventory[index].id == car["id"]) {
      return index;
    }
  }
}
console.log(indexOf(inv, car));
