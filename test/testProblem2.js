const lastCarDetails = require("../problem2");
const inventory = require("../inventory");
try {
  const result = lastCarDetails(inventory);
  console.log(result);
} catch (e) {
  console.log(e.message);
}
