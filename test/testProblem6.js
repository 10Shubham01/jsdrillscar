const getBMWAndAudi = require("../problem6");
const inventory = require("../inventory");
try {
  const cars = ["BMW", "Audi"];
  const result = getBMWAndAudi(inventory, cars);
  console.log(result);
} catch (e) {
  console.log(e.message);
}
