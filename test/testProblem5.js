const getOlderCar = require("../problem5");
const inventory = require("../inventory");

const getCarAllYears = require("../problem4");
try {
  const carYears = getCarAllYears(inventory);

  const result = getOlderCar(carYears);
  console.log(result.length);
} catch (e) {
  console.log(e.message);
}
