const getCarAllYears = require("../problem4");
const inventory = require("../inventory");
try {
  const result = getCarAllYears(inventory);
  console.log(result);
  module.exports = result; // exporting for problem5
} catch (e) {
  console.log(e.message);
}
