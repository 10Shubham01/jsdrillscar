const getCarDetails = require("../problem1");
const inventory = require("../inventory");
try {
  const result = getCarDetails(inventory);
  console.log(result);
} catch (e) {
  console.log(e.message);
}
