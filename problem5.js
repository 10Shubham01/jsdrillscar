const getOlderCar = (carYear) => {
  if (carYear) {
    let olderCars = [];

    for (let index = 0; index < carYear.length; index++) {
      if (carYear[index] < 2000) {
        olderCars.push(carYear[index]);
      }
    }
    return olderCars;
  }
};

module.exports = getOlderCar;
